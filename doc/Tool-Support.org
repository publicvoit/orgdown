★ [[Overview.org][← Overview page]] ★

* Orgdown Tool Support

This section summarizes Orgdown support from various tools. It is by
far *not a complete list* of tools that do have at least minimum
Orgdown support.

If you want to *add a tool* or update its assessment, [[Contribute.org][please do feel
free to do so]] via handing in a pull request or [[https://gitlab.com/publicvoit/orgdown/-/issues][issuing a ticket]]. The
[[tools/Template.org][template file]] will help you.

Numbers in the "OD1 Compatibility" columns represent [[Orgdown-Levels.org][the percentage of
OD1 compatibility]].

If there is no detailed Orgdown1 assessment yet, the compatibility is
guessed with either ✓ for basic support and ✓✓ for decent support.

** Generic

Every simple text editor does have at least some basic support for
Orgdown. As long as it respects line breaks, you can type Orgdown
syntax yourself.

| *Tool*                | *Platform* | *OD1 Compatibility* |
|-----------------------+------------+---------------------|
| Email                 | any        | (guess: ✓)          |
| Most editors          | any        | (guess: ✓)          |
| Pandoc file converter | any        | (guess: ✓✓)         |
|-----------------------+------------+---------------------|

Please note that there are [[https://orgmode.org/worg/org-blog-wiki.html][many different blog and wiki generators]] that are able to process Org-mode syntax to generate web pages.

The [[https://orgmode.org/worg/org-tools/index.html][org-tools page of Worg]] does not only list parsers. There are many
other Org-related tools you might find interesting.

On GitHub you will find [[https://github.com/topics/orgmode][many projects tagged with "orgmode"]].

You see, with Org-mode related tools, you also do have an entire
ecosystem of tools for Orgdown as well.

Go and get creative!

#+BEGIN_COMMENT
Please do move these to tool-specific pages on assessing the OD1 compatibility:

| *Tool*                | *Platform*       | *Text formatting* | *Lists* | *Blocks* | *Comments* | *Links* | *Tables* | *Note*                                                |   |
|-----------------------+------------------+-------------------+---------+----------+------------+---------+----------+-------------------------------------------------------+---|
| [[https://pandoc.org/][Pandoc]] file converter | any              | ✓✓                | ✓✓      | ✓✓       | ✓✓         | ✓✓      | ✓✓       | [[https://pandoc.org/try/?text=*+Syntax+Examples%0A%0A**+*Simple*+%2Ftext%2F+_formatting_%0A%0A-+*bold*%0A-+%2Fitalic%2F%0A-+_underline_%0A-+%2Bstrike+through%2B%0A-+%3Dcode%3D%0A-+~commands~%0A%0A%3A+small+example%0A%0AWithin+links%3A%0A%0A%5B%5Bhttp%3A%2F%2Forgmode.org%5D%5BThis+*is*+an+%2Fexample%2F+of+_some_+syntax+%2Bhighlighting%2B+within+%3Dlinks%3D+and+~such~.%5D%5D%0A%0A%5B%5Bhttps%3A%2F%2Fgnu.org%5D%5B~gnu~%5D%5D%0A%0A-----%0A%0AFive+or+more+dashes+produce+a+horizontal+line.%0A%0A**+Lists+and+Checkboxes%0A%0Asimple+list%3A%0A-+Org+mode%0A-+Lists%0A-+%5B+%5D+unchecked+checkbox%0A-+%5BX%5D+checked+checkbox%0A-+%5B-%5D+undecided+checkbox+(%3DC-u+C-u+C-c+C-c%3D)%0A%0Aenumerate%3A%0A1.+Emacs%0A2.+Org-mode%0A3.+Lists%0A%0Amixed+with+checkboxes%3A%0A%0A-+Clean+out+garage%0A++1.+%5B+%5D+get+stuff+out%0A+++++-+%5BX%5D+be+careful+with+that+axe%2C+Eugene%0A++2.+%5BX%5D+get+rid+of+old+stuff%0A+++++-+sell+on+the+Internet%3F%0A+++++-+try+to+use+rubbish+as+birthday+presents+for+family%0A++3.+%5B+%5D+repaint+garage%0A++4.+%5B+%5D+put+stuff+back+in%0A%0A**+Blocks%0A%0AAn+EXAMPLE+block+is+rendered+%22as+is%22%2C+keeping+line+breaks+and+not%0Ainterpreting+content%3A%0A%0A%23%2BBEGIN_EXAMPLE%0AAn+example+in+an+EXAMPLE+block.%0ASecond+line+within+this+block.%0A%0AThis+*is*+an+%2Fexample%2F+of+_some_+syntax+%2Bhighlighting%2B+within+%3Dlinks%3D+and+~such~.%0A%23%2BEND_EXAMPLE%0A%0AContent+within+a+QUOTE+block+may+get+different+line+breaks+when%0Aexported%2Frenderd+and+interprets+text+formatting%3A%0A%0A%23%2BBEGIN_QUOTE%0AAn+example+in+an+QUOTE+block.%0ASecond+line+within+this+block.%0A%0AThis+*is*+an+%2Fexample%2F+of+_some_+syntax+%2Bhighlighting%2B+within+%3Dlinks%3D+and+~such~.%0A%23%2BEND_QUOTE%0A%0ASRC+blocks+contain+source+code+snippets.+Text+formatting+is+ignored%2C%0Aline+breaks+preserved.%0A%0AA+Python+source+code+example%3A%0A%0A%23%2BBEGIN_SRC%0A++def+my_test(myvar%3A+str+%3D+%27foo+bar%27)%3A%0A++++++%22%22%22%0A++++++This+is+an+example+function.%0A%0A++++++%40type++myvar%3A+str+%3D+%27foo+bar%27%3A+number%0A++++++%40param+myvar%3A+str+%3D+%27foo+bar%27%3A+FIXXME%0A++++++%22%22%22%0A%0A++++++mynewvar%3A+str+%3D+myvar+%2B+%27+additional+content%27%0A++++++return+mynewvar%0A%0A++print(%22Hello+%22+%2B+my_text(%27Europe!%27))%0A%23%2BEND_SRC%0A%0AA+shell+script+example%3A%0A%0A%23%2BBEGIN_SRC%0Aecho+%22Hello+Europe!%22%0AFOO%3D%22foo+bar%22%0Aecho+%22A+test+with+%24%7BFOO%7D%22%0Apwd%0A%23%2BEND_SRC%0A%0A**+Comments%0A%0AComment++lines+contain++content+which++is+not++visible+in++any+derived%0Adocument+such+as+a+PDF+document+or+a+web+view.%0A%0Ahash+space%3A%0A%0A%23+This+is+a+comment%0A%0Aspace+hash+space%3A%0A%0A+%23+This+is+a+comment%0A%0Aspace+space+hash+space%3A%0A%0A++%23+This+is+a+comment%0A%0A-----------%0A%0AComment+block%3A%0A%0A%23%2BBEGIN_COMMENT%0AThis+is+a+multi+line+comment+block.%0AThis+is+the+second+line.%0A%0AThis+is+the+second+paragraph.%0A%0AThis+*is*+an+%2Fexample%2F+of+_some_+syntax+%2Bhighlighting%2B+within+%3Dlinks%3D+and+~such~.%0A%23%2BEND_COMMENT%0A%0A**+Links%0A%0A-+file%3Atest.org+without+brackets%0A-+%5B%5Bfile%3Atest.org%5D%5D+with+double+brackets%0A-+http%3A%2F%2Forgmode.org+%E2%86%92+plain+URL+without+brackets%0A-+%5B%5Bhttp%3A%2F%2Forgmode.org%5D%5D+%E2%86%92+URL+with+brackets+without+description%0A-+%5B%5Bhttp%3A%2F%2Forgmode.org%5D%5BOrg+mode+homepage%5D%5D+%E2%86%92+URL+with+brackets+with+description%0A%0A**+Tables%0A%0ATables+do+not+have+to+be+proper+aligned+as+long+as+the+correct+number%0Aof+vertical+bars+per+line+is+met.+Text+formatting+is+applied.%0A%0A%7C+*Heading1*+%7C+*head2*+%7C%0A%7C------------%2B---------%7C%0A%7C+entry++++++%7C++++++42+%7C%0A%7C+foo++++++++%7C++++21.7+%7C%0A%7C------------%2B---------%7C%0A%7C+end++++++++%7C+++9&from=org&to=gfm&standalone=0][Example]]; Using Org mode format for source or target   |   |
#+END_COMMENT

** Web Services

| *Tool*     | *Platform* | *OD1 Compatibility* |
|------------+------------+---------------------|
| [[Tools/Logseq.org][Logseq]] | Web        | 86%      |
| OrgModeWeb | Web        | (guess: ✓)          |
| [[tools/GitHub.org][GitHub]]     | Web        | 95%                 |
| [[tools/GitLab.org][GitLab]]     | Web        | 95%                 |
| filestash  | Web        | (guess: ✓✓)         |
| organice   | Web        | (guess: ✓✓)         |
|------------+------------+---------------------|

#+BEGIN_COMMENT
Please do move these to tool-specific pages on assessing the OD1 compatibility:

| *Tool*                | *Platform*       | *Text formatting* | *Lists* | *Blocks* | *Comments* | *Links* | *Tables* | *Note*                                                |   |
|-----------------------+------------------+-------------------+---------+----------+------------+---------+----------+-------------------------------------------------------+---|
| [[https://organice.200ok.ch/][organice]]              | Web              | ✓✓                | ✓✓      | ?        | ?          | ✓✓      | ✓✓       | [[https://organice.200ok.ch/sample][Example]]                                               |   |
| [[https://orgmodeweb.org/][OrgModeWeb]]            | Web              | ✓✓                | ✓       | ✓        | ✓          | ✓       | ✓        | [[https://github.com/borablanca/orgmodeweb][Source]]                                                |   |
| [[https://www.filestash.app/][filestash]]             | Web              | ✓✓                | ✓✓      | ✓✓       | ✓✓         | ✓✓      | ?        | [[https://github.com/mickael-kerjean/filestash][Source]]; [[https://www.filestash.app/2018/05/31/release-note-v0.1/][Example]]                                       |   |
#+END_COMMENT

** Web Page Generators

| *Tool*    | *Platform* | *OD1 Compatibility* |
|-----------+------------+---------------------|
| [[tools/lazyblorg.org][lazyblorg]] | Python3    |                 77% |

** Mobile Apps

| *Tool*      | *Platform*    | *OD1 Compatibility* |
|-----------+-------------+-------------------|
| [[tools/Orgro.org][Orgro]]     | Android/iOS |               88% |
| [[tools/PlainOrg.org][Plain Org]] | iOS         |               92% |
| [[tools/Orgzly.org][Orgzly]]    | Android     |               86% |
| beorg     | iOS         |                 ? |
| [[https://logseq.com/][Logseq]]    | Android/iOS |               86% |
| [[https://github.com/gsantner/markor][Markor]]    | Android     |                 ? |
|-----------+-------------+-------------------|

#+BEGIN_COMMENT
Please do move these to tool-specific pages on assessing the OD1 compatibility:

| *Tool*                | *Platform*       | *Text formatting* | *Lists* | *Blocks* | *Comments* | *Links* | *Tables* | *Note*                                                |   |
|-----------------------+------------------+-------------------+---------+----------+------------+---------+----------+-------------------------------------------------------+---|
| [[http://www.orgzly.com/][Orgzly]]                | Android          | ✓✓                | ✓       | ✓✓       | ✓✓         | ✓✓      | ✓        | [[https://github.com/orgzly/orgzly-android][Source]]                                                |   |
| [[https://orgro.org/][Orgro]]                 | Android/iOS      | ✓✓                | ✓       | ✓        | ✓          | ✓       | ✓        | mobile viewer; [[https://github.com/amake/orgro][Source]]; [[https://orgro.org/][Example]]                        |   |
| [[https://beorgapp.com/][beorg]]                 | iOS              | ?                 | ?       | ?        | ?          | ?       | ?        |                                                       |   |
#+END_COMMENT

** Win/macOS/Linux

| *Tool*       | *Platform*      | *OD1 Compatibility* |
|--------------+-----------------+---------------------|
| [[tools/Emacs.org][Emacs]]        | Win/macOS/Linux |                100% |
| nvim-orgmode | Win/macOS/Linux |                 77% |
| vim-orgmode  | Win/macOS/Linux |         (guess: ✓✓) |
| [[https://github.com/xwmx/nb][nb]]           | Win/macOS/Linux |                   ? |
| [[Tools/Logseq.org][Logseq]]       | Win/macOS/Linux |                 86% |
| [[tools/EasyOrg.org][EasyOrg]]      | Win/macOS/Linux |                  80 |
|--------------+-----------------+---------------------|

#+BEGIN_COMMENT
Please do move these to tool-specific pages on assessing the OD1 compatibility:

| *Tool*                | *Platform*       | *Text formatting* | *Lists* | *Blocks* | *Comments* | *Links* | *Tables* | *Note*                                                |   |
|-----------------------+------------------+-------------------+---------+----------+------------+---------+----------+-------------------------------------------------------+---|
| [[https://github.com/jceb/vim-orgmode][vim-orgmode]]           | Win/macOS/Linux  | ✓✓                | ✓✓      | ✓✓       | ✓✓         | ✓✓      | ✓✓       | for the vim editor; [[https://github.com/jceb/vim-orgmode/blob/master/examples/mylife.png][Example]]                           |   |
| [[https://github.com/nvim-orgmode/orgmode][nvim-orgmode]]      | Win/macOS/Linux  | ✓✓                | ✓✓      | ✓✓       | ✓✓         | ✓✓      | ✓✓       | Org mode clone for Neovim 0.5+                        |   |
#+END_COMMENT

** Tool-specific Add-ons

| *Tool*                   | *Platform*       | *OD1 Compatibility* |
|--------------------------+------------------+---------------------|
| [[https://braintool.org][BrainTool]]                | Chromium browser | (guess: ✓)          |
| Org Extended             | [[https://en.wikipedia.org/wiki/Sublime_Text][Sublime Text]]     | (guess: ✓✓)         |
| Org4Idea                 | [[https://en.wikipedia.org/wiki/IntelliJ_IDEA][IntelliJ IDEA]]    | ?                   |
| VS Marketplace Addons    | [[https://en.wikipedia.org/wiki/Microsoft_Visual_Studio][Visual Studio]]    | ?                   |
| [[https://atom.io/packages/org-mode][Org-mode for Atom]]        | Atom             | ?                   |
| [[https://github.com/danielmagnussons/orgmode][Orgmode for Sublime Text]] | Sublime Text     | ? |
| [[https://directory.getdrafts.com/s/1qu][Drafts' Org syntax]] | Drafts (macOS, iOS) | (guess: ✓)                |
|--------------------------+------------------+---------------------|

#+BEGIN_COMMENT
Please do move these to tool-specific pages on assessing the OD1 compatibility:

| *Tool*                | *Platform*       | *Text formatting* | *Lists* | *Blocks* | *Comments* | *Links* | *Tables* | *Note*                                                |   |
|-----------------------+------------------+-------------------+---------+----------+------------+---------+----------+-------------------------------------------------------+---|
| [[https://plugins.jetbrains.com/plugin/7095-org4idea][Org4Idea]]              | [[https://en.wikipedia.org/wiki/IntelliJ_IDEA][IntelliJ IDEA]]    | ?                 | ?       | ✓        | ?          | ?       | ?        |                                                       |   |
| [[https://marketplace.visualstudio.com/search?term=org%20mode&target=VSCode&category=All%20categories&sortBy=Relevance][VS Marketplace Addons]] | [[https://en.wikipedia.org/wiki/Microsoft_Visual_Studio][Visual Studio]]    | ?                 | ?       | ?        | ?          | ?       | ?        | Multiple extensions offer support for Org mode        |   |
| [[https://braintool.org/][BrainTool]]             | Chromium browser | ✓                 | ✓       | ✓        | ✓          | ✓✓      | ✓        | A topic manager replacing classic bookmark management |   |
| [[https://github.com/ihdavids/orgextended][Org Extended]]          | [[https://en.wikipedia.org/wiki/Sublime_Text][Sublime Text]]     | ✓✓                | ✓✓      | ✓✓       | ?          | ✓✓      | ✓✓       |                                                       |   |
#+END_COMMENT
