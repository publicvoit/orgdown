★ [[Overview.org][← Overview page]] ★

* Orgdown - Formal Syntax Definition

So far, there is no formal syntax definition for Orgdown1, which is the
first and most basic level of Orgdown syntax. [[Contribute.org][This might change in
the future]].

As you might already know, Orgdown is a sub-set of the mighty
[[https://orgmode.org/][Org-mode]]. Fortunately, there are multiple approaches in defining the
Org-mode syntax in a formal style. This way, *almost any basic formal
specification of Org-mode also covers Orgdown1*:

- [[https://orgmode.org/worg/dev/org-syntax.html][https://orgmode.org/worg/dev/org-syntax.html]]
- [[https://github.com/200ok-ch/org-parser/blob/master/resources/org.ebnf][https://github.com/200ok-ch/org-parser/blob/master/resources/org.ebnf]]

If you do have experience with Orgdown and specific definitions,
please do report back so that others are able to learn from you.
